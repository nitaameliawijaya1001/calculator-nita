package com.assessment79.nitaameliawijaya.lesson_calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    TextView inputText1;
    TextView inputText2;
    TextView inputText3;

    boolean isStatusKalkulasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isStatusKalkulasi = false;
        inputText1 = findViewById(R.id.input_1);
        inputText2 = findViewById(R.id.input_2);
        inputText3 = findViewById(R.id.input_3);

        initButton1();
        initButton2();
        initButton3();
        initButton3();
        initButton4();
        initButton5();
        initButton6();
        initButton7();
        initButton8();
        initButton9();
        initButton0();
        initButtonAdd();
        initButtonMinus();
        initButtonDiv();
        initButtonMultiple();
        initButtonEqual();
        initButtonPoint();
        initButtonClear();
        initButtonDelete();
    }

    public void initButton1(){
        Button button1 = findViewById(R.id.button_1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText2 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("1");
                }else if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    showInputText3("1");
                }else{
                    showInputText1("1");
                }


            }
        });
    }

    public void initButton2(){
        Button button2 = findViewById(R.id.button_2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText2 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("2");
                }else if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    showInputText3("2");
                }else{
                    showInputText1("2");
                }
            }
        });
    }

    public void initButton3(){
        Button button3 = findViewById(R.id.button_3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText2 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("3");
                }else if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    showInputText3("3");
                }else{
                    showInputText1("3");
                }
            }
        });
    }

    public void initButton4(){
        Button button4 = findViewById(R.id.button_4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText1 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("4");
                }else if(tempInputText1 != null && !tempInputText1.isEmpty()){
                    showInputText3("4");
                }else{
                    showInputText1("4");
                }
            }
        });
    }

    public void initButton5(){
        Button button5 = findViewById(R.id.button_5);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText2 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("5");
                }else if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    showInputText3("5");
                }else{
                    showInputText1("5");
                }
            }
        });
    }

    public void initButton6(){
        Button button6 = findViewById(R.id.button_6);
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText2 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("6");
                }else if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    showInputText3("6");
                }else{
                    showInputText1("6");
                }
            }
        });
    }

    public void initButton7(){
        Button button7 = findViewById(R.id.button_7);
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText2 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("7");
                }else if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    showInputText3("7");
                }else{
                    showInputText1("7");
                }
            }
        });
    }

    public void initButton8(){
        Button button8 = findViewById(R.id.button_8);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText2 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("8");
                }else if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    showInputText3("8");
                }else{
                    showInputText1("8");
                }
            }
        });
    }

    public void initButton9(){
        Button button9 = findViewById(R.id.button_9);
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText2 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("9");
                }else if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    showInputText3("9");
                }else{
                    showInputText1("9");
                }
            }
        });
    }

    public void initButton0(){
        Button button0 = findViewById(R.id.button_0);
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tempInputText2 = inputText2.getText()+"";
                if(isStatusKalkulasi == true){
                    inputText1.setText("");
                    inputText2.setText("");
                    inputText3.setText("");
                    isStatusKalkulasi = false;
                    showInputText1("0");
                }else if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    showInputText3("0");
                }else{
                    showInputText1("0");
                }
            }
        });
    }

    public void initButtonClear(){
        Button buttonClear = findViewById(R.id.button_clear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputText1.setText("");
                inputText2.setText("");
                inputText3.setText("");
            }
        });
    }

    public void initButtonPoint(){
        Button buttonPoint = findViewById(R.id.button_point);
        buttonPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInputText1 = inputText1.getText()+"";
                String tempInputText2 = inputText2.getText()+"";
                String tempInputText3 = inputText3.getText()+"";

                Pattern checkPattern = Pattern.compile("^[^\\.]*$");

                if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    Matcher checkValue = checkPattern.matcher(tempInputText3);
                    if(checkValue.find()){
                        if(tempInputText3.equals("")){
                            inputText3.setText("0.");
                        }else{
                            inputText3.setText(tempInputText3+".");
                        }
                    }else{
                        inputText3.setText(tempInputText3+"");
                    }
                }else{
                    Matcher checkValue = checkPattern.matcher(tempInputText1);
                    if(checkValue.find()){
                        if(tempInputText1.equals("")){
                            inputText1.setText("0.");
                        }else{
                            inputText1.setText(tempInputText1+".");
                        }
                    }else{
                        inputText1.setText(tempInputText1+"");
                    }
                }
            }
        });
    }

    public void initButtonDelete(){
        Button buttonDelete = findViewById(R.id.button_delete);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempInputText1 = inputText1.getText()+"";
                String tempInputText2 = inputText2.getText()+"";
                String tempInputText3 = inputText3.getText()+"";

                if(tempInputText2 != null && !tempInputText2.isEmpty()){
                    if(tempInputText3 != null && !tempInputText3.isEmpty()){
                        tempInputText3 = tempInputText3.substring(0,tempInputText3.length()-1);
                        inputText3.setText(tempInputText3);
                    }else{
//                        tempInputText2 = tempInputText2.substring(0,tempInputText3.length()-1);
                        inputText2.setText("");
                    }
                }else{
                    if(tempInputText1 != null && !tempInputText1.isEmpty()){
                        tempInputText1 = tempInputText1.substring(0,tempInputText1.length()-1);
                        inputText1.setText(tempInputText1);
                    }else{
                        Toast.makeText(getApplicationContext(), "Terhapus!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void initButtonAdd(){
        final Button buttonAdd = findViewById(R.id.button_plus);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringDataText1 = inputText1.getText()+"";
                String stringDataText3 = inputText3.getText()+"";

                if(stringDataText3 != null && !stringDataText3.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Operator sudah terisi!", Toast.LENGTH_SHORT).show();
                }else if(stringDataText1.equals("")){
                    Toast.makeText(getApplicationContext(), "Kolom 1 harus diisi dahulu!", Toast.LENGTH_SHORT).show();
                }else{
                    showInputText2("+");
                }

            }
        });
    }

    public void initButtonMinus(){
        Button buttonMinus = findViewById(R.id.button_minus);
        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringDataText1 = inputText1.getText()+"";
                String stringDataText2 = inputText2.getText()+"";
                String stringDataText3 = inputText3.getText()+"";

                if(stringDataText3 != null && !stringDataText3.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Operator sudah terisi!", Toast.LENGTH_SHORT).show();
                }else if(stringDataText1 != null && !stringDataText1.isEmpty()){
                    if(stringDataText2 != null && !stringDataText2.isEmpty()){
                        if(stringDataText3.equals("")){
                            showInputText3("-");
                        }
                    }else{
                        showInputText2("-");
                    }
                }else if(stringDataText1.equals("")){
                    showInputText1("-");
                }else{
                    showInputText2("-");
                }
            }
        });
    }

    public void initButtonMultiple(){
        Button buttonMultiple = findViewById(R.id.button_multiple);
        buttonMultiple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringDataText1 = inputText1.getText()+"";
                String stringDataText3 = inputText3.getText()+"";

                if(stringDataText3 != null && !stringDataText3.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Operator sudah terisi!", Toast.LENGTH_SHORT).show();
                }else if(stringDataText1.equals("")){
                    Toast.makeText(getApplicationContext(), "Kolom 1 harus diisi dahulu!", Toast.LENGTH_SHORT).show();
                }else{
                    showInputText2("*");
                }
            }
        });
    }

    public void initButtonDiv(){
        Button buttonDiv = findViewById(R.id.button_div);
        buttonDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringDataText1 = inputText1.getText()+"";
                String stringDataText3 = inputText3.getText()+"";

                if(stringDataText3 != null && !stringDataText3.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Operator sudah terisi!", Toast.LENGTH_SHORT).show();
                }else if(stringDataText1.equals("")){
                    Toast.makeText(getApplicationContext(), "Kolom 1 harus diisi dahulu!", Toast.LENGTH_SHORT).show();
                }else{
                    showInputText2("/");
                }
            }
        });
    }

    public void initButtonEqual(){
        Button buttonEqual = findViewById(R.id.button_equal);
        buttonEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringDataText1 = inputText1.getText()+"";
                String stringDataText2 = inputText2.getText()+"";
                String stringDataText3 = inputText3.getText()+"";

                if(stringDataText1 != null && !stringDataText1.isEmpty()){
                    if(stringDataText3 != null && !stringDataText3.isEmpty()){
                        Float intDataText1 = Float.parseFloat(stringDataText1);
                        Float intDataText3 = Float.parseFloat(stringDataText3);

                        Float resultFloat;
                        if(stringDataText2.equals("+")){
                            resultFloat = intDataText1 + intDataText3;
                            showResult(resultFloat);
                        }else if(stringDataText2.equals("-")){
                            resultFloat = intDataText1 - intDataText3;
                            showResult(resultFloat);
                        }else if(stringDataText2.equals("*")){
                            resultFloat = intDataText1 * intDataText3;
                            showResult(resultFloat);
                        }else if(stringDataText2.equals("/")){
                            resultFloat = intDataText1 /intDataText3;
                            showResult(resultFloat);
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Kolom 3 belum terisi!", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Terhapus!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void showResult(Float value){
        if(value % 1 != 0){
            String resultString1 = value + "";
            inputText1.setText("");
            inputText2.setText("");
            inputText3.setText("");
            inputText1.setText(resultString1+"");
            isStatusKalkulasi = true;
        }else{
            Integer tempResult = Math.round(value);
            String resultString2 = tempResult+"";
            inputText1.setText("");
            inputText2.setText("");
            inputText3.setText("");
            inputText1.setText(resultString2+"");
            isStatusKalkulasi = true;
        }
    }

    public void showInputText1(String value){
        String tempInputText1 = inputText1.getText()+"";
        if(tempInputText1.equals("0")){
            inputText1.setText(value+"");
        }else{
            inputText1.setText(inputText1.getText()+value+"");
        }
    }

    public void showInputText2(String value){
        inputText2.setText(value+"");
    }

    public void showInputText3(String value){
        String tempInputText3 = inputText3.getText()+"";
        if(tempInputText3.equals("0")){
            inputText3.setText(value+""); 
        }else{
            inputText3.setText(inputText3.getText()+value+"");
        }
    }
}
